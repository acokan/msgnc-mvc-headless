
import React, {Component} from 'react';
import InternList from './InternList';
import { connect } from 'react-redux';
import {actionGetInterns} from '../actions/actionGetInterns';
import {interns} from '../data/interns';
import Loading from './Loading';

class Home extends Component {
    
    componentWillMount() {
        this.props.actionGetInterns();
    }

    _openDetailsPage = (intern) => {
        this.props.history.push(`/intern/${intern.id}`)
    }

    render() {
        const {interns} = this.props;

        if (!interns) {
            return <Loading />
        }
        return (
            <section className="app__section">
                <InternList interns={interns} openDetailsPage={this._openDetailsPage} />
            </section>
        );
    }
}

export default connect(state => {
    return {
        interns: state.interns
    };
}, {
    actionGetInterns
})(Home);
