
export function loadingReducer(baseEvents) {
    if (typeof baseEvents === 'string') {
        baseEvents = [baseEvents];
    }

    return function(state = null, action) {
        for (let i = 0; i < baseEvents.length; i++) {
            const evt = baseEvents[i];

            if (evt === action.type) {
                return action.data || '...';
            }
            if (action.type === `${evt}_SUCCESS` || action.type === `${evt}_FAILED`) {
                return null;
            }
        }
        return state;
    };
}


export function errorReducer(baseEvents, cfg) {
    const {resetActions} = Object.assign({
        resetActions: []
    }, cfg);
    if (typeof baseEvents === 'string') {
        baseEvents = [baseEvents];
    }

    return function(state = null, action) {
        if (resetActions.indexOf(action.type) !== -1) {
            return null;
        }
        for (let i = 0; i < baseEvents.length; i++) {
            const evt = baseEvents[i];
            if (action.type === evt || action.type === `${evt}_SUCCESS`) {
                return null;
            }
            if (action.type === `${evt}_FAILED`) {
                return action.data || true;
            }
        }
        return state;
    };
}


export function mainReducer(baseEvents, cfg) {
    const {resetActions} = Object.assign({
        resetActions: []
    }, cfg);
    if (typeof baseEvents === 'string') {
        baseEvents = [baseEvents];
    }

    return function(state = null, action) {
        if (resetActions.indexOf(action.type) !== -1) {
            return null;
        }
        for (let i = 0; i < baseEvents.length; i++) {
            const evt = baseEvents[i];
            if (action.type === evt || action.type === `${evt}_FAILED`) {
                return null;
            }
            if (action.type === `${evt}_SUCCESS`) {
                return action.data;
            }
        }
        return state;
    };
}
