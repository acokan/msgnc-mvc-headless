import React, {Component} from 'react';
import ReactPropTypes from 'prop-types';

const {
    object
} = ReactPropTypes;

class Intern extends Component {
    static propTypes = {
        intern: object
    }

    _openDetailsPage = (e) => {
        e.preventDefault();
        this.props.openDetailsPage(this.props.intern);
    }

    render() {
        const {intern} = this.props;

        return (
            <div className={`col-xs-12 intern__card`}>
                <div className="intern">
                    <img className="intern__image" src={intern.image} alt="Intern" />
                    <div className="intern__info">
                        <h3 className="intern__title">{`${intern.name} ${intern.surname}`}</h3>
                        <div>Score: {intern.score}</div>
                        <div>Position: {intern.position}</div>
                    </div>
                    <div className="intern__buttons">
                        <button className="btn btn-block btn-msgNC" onClick={this._openDetailsPage}>
                            Details
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Intern;
