import {mainReducer, loadingReducer, errorReducer} from './main';
import ActionTypes from '../actiontypes';

export const interns = mainReducer(ActionTypes.GET_INTERNS);
export const internsLoading = loadingReducer(ActionTypes.GET_INTERNS);
export const internsError = errorReducer(ActionTypes.GET_INTERNS);

export const intern = mainReducer(ActionTypes.GET_INTERN);
export const internLoading = loadingReducer(ActionTypes.GET_INTERN);
export const internError = errorReducer(ActionTypes.GET_INTERN);

