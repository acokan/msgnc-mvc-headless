/* eslint-disable */
import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from '../components/Home';
import InternDetail from '../components/InternDetail';

export default function routesFactory(store) {
    const routes = (
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path={"/intern/:id"} component={InternDetail} />
        </Switch>
    );
    return routes;
}
