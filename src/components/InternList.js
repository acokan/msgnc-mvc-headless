import React, {Component} from 'react';
import ReactPropTypes from 'prop-types';
import Intern from './Intern';

const {
    object
} = ReactPropTypes;

class InternList extends Component {
    static propTypes = {
        intern: object
    }

    _openDetailsPage = (intern) => {
        this.props.openDetailsPage(intern);
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-12 interns">
                    {
                        this.props.interns.map(intern =>
                            <Intern
                                key={intern.id}
                                intern={intern}
                                openDetailsPage={this._openDetailsPage} />
                        )
                    }
                </div>
            </div>
        );
    }
}

export default InternList;
