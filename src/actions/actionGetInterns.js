import {executeGet} from '../helpers/api'

export function actionGetInterns() {
    return executeGet({
        url: '/interns',
        actionType: 'GET_INTERNS'
    });
}

export function actionGetIntern(id) {
    return executeGet({
        url: `/intern/${id}`,
        actionType: 'GET_INTERN'
    });
}
