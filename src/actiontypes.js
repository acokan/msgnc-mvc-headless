function generate(prefix) {
    const result = {};
    result[prefix] = prefix;
    result[`${prefix}_SUCCESS`] = `${prefix}_SUCCESS`;
    result[`${prefix}_FAILED`] = `${prefix}_FAILED`;
    return result;
}

const groups = [
    'GET_INTERNS',
    'GET_INTERN'
];

export default Object.assign({}, groups.reduce((result, group) => {
    return Object.assign({}, result, generate(group));
}, {}));
