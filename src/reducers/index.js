import * as interns from './internsReducer';

function createExport(reducers) {
    return reducers.reduce((returnObj, reducer) => {
        return Object.assign(returnObj, Object.keys(reducer).reduce((reducerObj, reducerKey) => {
            reducerObj[reducerKey] = reducer[reducerKey];
            return reducerObj;
        }, {}));
    }, {});
}

export default Object.assign({

}, createExport([
    interns
]));
