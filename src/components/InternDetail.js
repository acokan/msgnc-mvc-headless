import React, {Component} from 'react';
import { connect } from 'react-redux';
import ReactPropTypes from 'prop-types';
import { actionGetIntern } from '../actions/actionGetInterns';
import Loading from './Loading';


const {
    object
} = ReactPropTypes;

class InternDetail extends Component {
    static propTypes = {
        intern: object
    }

    componentWillMount() {
        const {match: {params}, actionGetIntern} = this.props;
        if (params.id) {
            actionGetIntern(params.id);
        }
    }

    render() {
        const {intern} = this.props;

        if (!intern) {
            return <Loading />
        }
        
        return (
            <div className="col-sm-12 ">
                <div className="intern">
                    <img className="intern__image" src={intern.image} alt="Intern" />
                    <div className="intern__info">
                        <h3 className="intern__title">{`${intern.name} ${intern.surname}`}</h3>
                        <div>Score: {intern.score}</div>
                        <div>Position: {intern.position}</div>
                    </div>
                    <div className="intern__description">
                        Contrary to popular belief, Lorem Ipsum is not simply random text. 
                        It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. 
                        Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure 
                        Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, 
                        discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" 
                        (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular 
                        during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                    </div>
                    <button className="btn btn-msgNC" onClick={() => this.props.history.push('/')}>Back</button>
                </div>
            </div>
        );
    }
}

export default connect(state => {
    return {
        intern: state.intern,
    };
}, {
    actionGetIntern
})(InternDetail);
