export const interns = [
    {
        id: 1,
        name: 'Marko',
        surname: 'Jovanovic',
        image: '/images/01.png',
        position: 'Frontend',
        score: 78
    },
    {
        id: 2,
        name: 'Sanja',
        surname: 'Popovic',
        image: '/images/02.png',
        position: 'Backend',
        score: 93
    },
    {
        id: 3,
        name: 'Milan',
        surname: 'Krsticic',
        image: '/images/03.png',
        position: 'Consultant',
        score: 81
    },
    {
        id: 4,
        name: 'Jovana',
        surname: 'Milanovic',
        image: '/images/04.png',
        position: 'Frontend',
        score: 90
    },
    {
        id: 5,
        name: 'Sinisa',
        surname: 'Petkovic',
        image: '/images/05.png',
        position: 'Backend',
        score: 85
    },
    {
        id: 6,
        name: 'Dejana',
        surname: 'Savic',
        image: '/images/06.png',
        position: 'Consultant',
        score: 72
    },
    {
        id: 7,
        name: 'Nemanja',
        surname: 'Kostic',
        image: '/images/07.png',
        position: 'Frontend',
        score: 70
    },
    {
        id: 8,
        name: 'Andjela',
        surname: 'Pesic',
        image: '/images/08.png',
        position: 'Backend',
        score: 98
    }
];
