const domain = "http://p85ee.mocklab.io";

function getDefaultSettings() {
    return {
        // credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json, text/plain, */*'
        }
    };
}

export function createSearchParams(obj) {
    const params = new URLSearchParams();
    Object.keys(obj).forEach(k => {
        params.append(k, obj[k]);
    });
    return params;
}

export function send(url, settings) {
    const finalSettings = Object.assign({}, getDefaultSettings(), settings);
    if (finalSettings.noContentTypeHeader) {
        delete finalSettings.headers['Content-Type'];
    }
    if (finalSettings.noAuthorizationHeader) {
        delete finalSettings.headers['Authorization'];
    }
    let faked = false;
    let u = url;
    if (typeof url === 'string') {
        if (url.indexOf('http') === 0) {
            u = new URL(url);
        } else {
            u = new URL(url, 'https://_');
            faked = true;
        }
    }
    if ('searchParams' in finalSettings) {
        const params = finalSettings.searchParams.toString();
        if (params && params !== '?') {
            u.search = `?${params.toString()}`;
        }
    }
    if (faked) {
        u = u.toString().substring(9);
    } else {
        u = u.toString();
    }
    return fetch(u, finalSettings).then(resp => {
        return new Promise((resolve, reject) => {
            if (!resp.ok) {
                return resp.json().then(errResp => {
                    return reject(errResp);
                }, (err) => {
                    return reject(err);
                });
            }


            if (finalSettings.noResponseBody) {
                return resolve(resp);
            }
            resp.json().then(data => {
                if (finalSettings.includeResponse) {
                    return resolve([data, resp]);
                } else {
                    return resolve(data);
                }
            }, reject);
        });
    });
};


export function executeGet(cfg) {
    const {url, actionType, sendConfig, postProcessor} = cfg;
    let pp = postProcessor;
    if (!pp) {
        pp = (data) => data;
    }
    return dispatch => {
        dispatch({
            type: actionType
        });
        console.log(url);
        send(`${domain}${url}`, sendConfig).then(data => {
            dispatch({
                type: `${actionType}_SUCCESS`,
                data: pp(data)
            });
        }, err => {
            console.log(err);
            dispatch({
                type: `${actionType}_FAILED`,
                data: err
            });
        });
    };
}
