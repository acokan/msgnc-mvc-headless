import React, {Component} from 'react';
import ReactPropTypes from 'prop-types';

class App extends Component {
    static propTypes = {
        children: ReactPropTypes.object,
    }

    render() {
        return (
            <div className="app">
                <header className="col-md-12">
                    <h2 className="app__title">Top Talent Program - Planning Tool</h2>
                </header>
                <div className="container">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default App;
