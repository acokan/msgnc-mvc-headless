import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux'
import thunk from 'redux-thunk';
import {Router, Route} from 'react-router-dom';
import routesFactory from './routes';
import createBrowserHistory from 'history/createBrowserHistory';

import App from './App';
import reducers from './reducers';
import './index.css';

const store = createStore(
    combineReducers(Object.assign({}, reducers)),
    applyMiddleware(thunk)
);

window.store = store;


const history = createBrowserHistory();
const routes = routesFactory(store);

ReactDOM.render(
    <Provider store={store}>
        <Router key="root" history={history} component={App}>
            <Route path="/" render={props => <App {...props}>{routes}</App>} />
        </Router>
    </Provider>,
    document.getElementById('root')
);
